package ru.knaus_g.imaginarium.ui.additional;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import ru.knaus_g.imaginarium.model.AdditionalObject;
import ru.knaus_g.imaginarium.room.DataBase;


public class AdditionalActivityModel extends ViewModel {


    private final DataBase dataBase;

    @Inject
    public AdditionalActivityModel(DataBase dataBase) {
        this.dataBase = dataBase;
    }

    @NonNull
    LiveData<List<AdditionalObject>> getLiveData(){
        return dataBase.daoAdditionalObject().getAllContent();
    }


}
