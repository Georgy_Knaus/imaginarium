package ru.knaus_g.imaginarium.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.Objects;

import ru.knaus_g.imaginarium.R;

public class DialogEndGame extends AppCompatDialogFragment {

    @Nullable
    private DialogEndGameListener dialogEndGameListener;
    private TextView textTitle;
    private TextView textMessage;
    private Button buttonNO;
    private Button buttonYES;
    private ImageView imageIcon;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_delete_gamer,
                getActivity().findViewById(R.id.layoutDialogDeleteGamer));

        builder.setView(view);

        textTitle = view.findViewById(R.id.textTitle);
        textTitle.setText("Конец игры");

        textMessage = view.findViewById(R.id.textMessage);
        textMessage.setText("Вы действительно хотите закончить игру?");

        buttonNO = view.findViewById(R.id.buttonNO);
        buttonNO.setText(getString(R.string.cancel));

        buttonYES = view.findViewById(R.id.buttonYES);
        buttonYES.setText("Да");

        imageIcon = view.findViewById(R.id.imageIcon);
        imageIcon.setImageResource(R.drawable.ic_warning_black_24dp);

        builder.setView(view);

        buttonNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        buttonYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogEndGameListener != null) {
                    dialogEndGameListener.DialogEndGamePositiveClick();
                }
                dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();

        if(alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }


        return alertDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            dialogEndGameListener = (DialogEndGame.DialogEndGameListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public interface DialogEndGameListener{
        void DialogEndGamePositiveClick();
    }
}
