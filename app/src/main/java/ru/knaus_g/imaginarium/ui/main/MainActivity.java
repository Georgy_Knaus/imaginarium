package ru.knaus_g.imaginarium.ui.main;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import ru.knaus_g.imaginarium.model.Gamer;
import ru.knaus_g.imaginarium.ui.about.AboutActivity;
import ru.knaus_g.imaginarium.ui.additional.AdditionalActivity;
import ru.knaus_g.imaginarium.ui.regulations.RegulationsActivity;
import ru.knaus_g.imaginarium.R;
import ru.knaus_g.imaginarium.viewmodels.ViewModelProviderFactory;

public class MainActivity extends DaggerAppCompatActivity implements DialogAddGamer.DialogAddGamerListener, DialogDeleteGamer.DialogDeleteListener, DialogEndGame.DialogEndGameListener {


    @Inject
    ViewModelProviderFactory providerFactory;
    @Inject
    MainAdapter adapter;

    private Button buttonStartGame, buttonCircle, buttonEndGame;
    private FloatingActionButton fab;
    private MainModel mainModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mainModel = ViewModelProviders.of(this, providerFactory).get(MainModel.class);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitle(R.string.name);


        fab = findViewById(R.id.fab);
        fab.setVisibility(View.INVISIBLE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogAddGamer myDialogFragmentDelete = new DialogAddGamer(adapter.getAllGamers());
                myDialogFragmentDelete.show(getSupportFragmentManager(), "DialogAddGamer");
            }
        });


        mainModel.getSingleLiveEvent().observe(this, new Observer<Gamer>() {
            @Override
            public void onChanged(Gamer gamer) {
                DialogDeleteGamer myDialogDeleteGamer = new DialogDeleteGamer();
                Bundle bundle = new Bundle();
                bundle.putInt("id", gamer.getId());
                bundle.putString("name", gamer.getName());
                myDialogDeleteGamer.setArguments(bundle);
                myDialogDeleteGamer.show(getSupportFragmentManager(), "DialogDeleteGamer");
            }
        });

        mainModel.getMLiveData().observe(this, new Observer<List<Gamer>>() {
            @Override
            public void onChanged(List<Gamer> gamers) {
                adapter.addAll(gamers);
            }
        });



        buttonCircle = findViewById(R.id.buttonCircle);
        buttonCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    mainModel.setScore();
            }
        });


        buttonEndGame = findViewById(R.id.buttonEndGame);
        buttonEndGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogEndGame dialogEndGame = new DialogEndGame();
                dialogEndGame.show(getSupportFragmentManager(), "DialogEndGame");
            }
        });

        buttonStartGame = findViewById(R.id.buttonStartGame);
        buttonStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAddGamer myDialogFragmentDelete = new DialogAddGamer(adapter.getAllGamers());
                myDialogFragmentDelete.show(getSupportFragmentManager(), "DialogAddGamer");
            }
        });


        // RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recyclerViewGamers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


        adapter.setAdapterListener(new MainAdapter.AdapterListener() {
            @Override
            public void onItemLongClicked(@NonNull Gamer gamer) {
               mainModel.setValueLiveEvent(gamer);
            }

            @Override
            public void afterTextChanged(String name, int points) {
                // Назначить промежуточные очки игроку
                mainModel.setAddPoints(name, points);
            }
        });


        if(mainModel.getGamer() != null){
            if(mainModel.getGamer().getName() != null){
                fab.setVisibility(View.VISIBLE);
                buttonCircle.setVisibility(View.VISIBLE);
                buttonEndGame.setVisibility(View.VISIBLE);
                buttonStartGame.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case R.id.actionAboutProgrammer:
                Intent intentAboutProgrammer = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(intentAboutProgrammer);
                return true;

            case R.id.actionRegulations:
                Intent intent = new Intent(MainActivity.this, RegulationsActivity.class);
                startActivity(intent);
                return true;

            case R.id.actionContent:
                Intent intentAdditionalContent = new Intent(MainActivity.this, AdditionalActivity.class);
                startActivity(intentAdditionalContent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void DialogAddGamerPositiveClick(String name, String color) {

        mainModel.addGamer(new Gamer(name, color));
        adapter.add(new Gamer(name, color));

        if (adapter.getItemCount() < 7) {
            DialogAddGamer myDialogFragmentDelete = new DialogAddGamer(adapter.getAllGamers());
            myDialogFragmentDelete.show(getSupportFragmentManager(), "DialogAddGamer");
            fab.setVisibility(View.VISIBLE);
            buttonCircle.setVisibility(View.VISIBLE);
            buttonEndGame.setVisibility(View.VISIBLE);
            buttonStartGame.setVisibility(View.INVISIBLE);
        } else{
            fab.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void DialogDeletePositiveClick(int id) {
        mainModel.deleteItem(id);
        if(adapter.getItemCount() == 1){
            fab.setVisibility(View.INVISIBLE);
            buttonCircle.setVisibility(View.INVISIBLE);
            buttonEndGame.setVisibility(View.INVISIBLE);
            buttonStartGame.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void DialogEndGamePositiveClick() {
        mainModel.deleteAll();

        fab.setVisibility(View.INVISIBLE);
        buttonCircle.setVisibility(View.INVISIBLE);
        buttonEndGame.setVisibility(View.INVISIBLE);
        buttonStartGame.setVisibility(View.VISIBLE);

        Intent intent = new Intent("android.intent.action.RESULT");
        intent.putExtra("winner", adapter.getResultGame());
        startActivity(intent);
    }
}
