package ru.knaus_g.imaginarium.ui.main;

import androidx.recyclerview.widget.DiffUtil;

import java.util.ArrayList;
import java.util.List;

import ru.knaus_g.imaginarium.model.Gamer;

public class DiffUtilCallback extends DiffUtil.Callback {

    private final List<Gamer> oldList = new ArrayList<>();
    private final List<Gamer> newList = new ArrayList<>();

    void setItems(List<Gamer> oldItems, List<Gamer> newItems) {
        oldList.clear();
        oldList.addAll(oldItems);

        newList.clear();
        newList.addAll(newItems);
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        Gamer oldProduct = oldList.get(oldItemPosition);
        Gamer newProduct = newList.get(newItemPosition);
        return oldProduct.getId() == newProduct.getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        Gamer oldProduct = oldList.get(oldItemPosition);
        Gamer newProduct = newList.get(newItemPosition);
        return oldProduct.equals(newProduct);
    }
}
