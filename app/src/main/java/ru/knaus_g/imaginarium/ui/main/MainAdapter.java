package ru.knaus_g.imaginarium.ui.main;

import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.knaus_g.imaginarium.R;
import ru.knaus_g.imaginarium.model.Gamer;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {


    @NonNull
    private List<Gamer> arrayGamer = new ArrayList<>();
    @Nullable
    private AdapterListener adapterListener;
    @NonNull
    private final DiffUtilCallback diffUtilCallback = new DiffUtilCallback();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        final ViewHolder vh = new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gamer_adapter, parent, false)
        );

        vh.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (adapterListener != null) {
                    adapterListener.onItemLongClicked(arrayGamer.get(vh.getAdapterPosition()));
                }
                return true;
            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.bind(arrayGamer.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayGamer.size();
    }


    void addAll(@NonNull List<Gamer> list){
        diffUtilCallback.setItems(arrayGamer, list);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffUtilCallback, true);
        arrayGamer.clear();
        arrayGamer.addAll(list);
        diffResult.dispatchUpdatesTo(this);
    }

    void add(Gamer gamer){
        arrayGamer.add(gamer);
    }


    List<Gamer> getAllGamers(){
        return arrayGamer;
    }


    String getResultGame(){
        StringBuilder result = new StringBuilder();

        Gamer.GamerSort gamerSort = new Gamer.GamerSort();
            Collections.sort(arrayGamer, gamerSort);

            int count_gamer = getItemCount();

            for(int i = 1; i<=count_gamer; i++){
                if(i==1){
                    result.append("Победитель:\n").append(arrayGamer.get(arrayGamer.size()-i).getName()).append(" счет - ").append(arrayGamer.get(arrayGamer.size()-i).getScore()).append("\n\n");
                    result.append("Далее:\n");
                } else{
                    if(i == count_gamer){
                        result.append(arrayGamer.get(arrayGamer.size()-i).getName()).append(" счет - ").append(arrayGamer.get(arrayGamer.size()-i).getScore()).append("\n\n");
                        result.append("Спасибо, что воспользовались приложением Имаджинариум!");
                    } else{
                        result.append(arrayGamer.get(arrayGamer.size()-i).getName()).append(" счет - ").append(arrayGamer.get(arrayGamer.size()-i).getScore()).append("\n");
                    }
                }
            }
        result.append("\n\n");

        return result.toString();
    }


    Gamer getGamer(int position){
        return arrayGamer.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout linearLayout;
        private TextView textName;
        private TextView textScore;
        private TextView textPlace;
        private NumberPicker numberPicker;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.textName);
            linearLayout = itemView.findViewById(R.id.itemGamer);
            textScore = itemView.findViewById(R.id.textScore);
            textPlace = itemView.findViewById(R.id.textPlace);
            numberPicker = itemView.findViewById(R.id.numberPicker);
        }

        void bind(Gamer gamer){
            textName.setText(gamer.getName());

            gamer.setNumberPicker(numberPicker);
            gamer.getNumberPicker().setValue(0);
            numberPicker.setMinValue(0);
            numberPicker.setMaxValue(9);

            switch (gamer.getColor()){
                case "#EF6C00":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_orange);
                    textName.setTextColor(Color.parseColor("#FFFFFF")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#FFFFFF"));
                    textPlace.setTextColor(Color.parseColor("#FFFFFF"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#FFFFFF"));
                    break;
                case "#2196f3":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_blue);
                    textName.setTextColor(Color.parseColor("#FFFFFF")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#FFFFFF"));
                    textPlace.setTextColor(Color.parseColor("#FFFFFF"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#FFFFFF"));
                    break;
                case "#212121":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_black);
                    textName.setTextColor(Color.parseColor("#FFFFFF")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#FFFFFF"));
                    textPlace.setTextColor(Color.parseColor("#FFFFFF"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#FFFFFF"));
                    break;
                case "#FFFFFF":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_white);
                    textName.setTextColor(Color.parseColor("#212121")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#212121"));
                    textPlace.setTextColor(Color.parseColor("#212121"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#212121"));
                    break;
                case "#b71c1c":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_red);
                    textName.setTextColor(Color.parseColor("#FFFFFF")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#FFFFFF"));
                    textPlace.setTextColor(Color.parseColor("#FFFFFF"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#FFFFFF"));
                    break;
                case "#FFEB3B":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_yellow);
                    textName.setTextColor(Color.parseColor("#FFFFFF")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#FFFFFF"));
                    textPlace.setTextColor(Color.parseColor("#FFFFFF"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#FFFFFF"));
                    break;
                case "#388E3C":
                    linearLayout.setBackgroundResource(R.drawable.layout_bg_green);
                    textName.setTextColor(Color.parseColor("#FFFFFF")); // Белый - #FFFFFF
                    textScore.setTextColor(Color.parseColor("#FFFFFF"));
                    textPlace.setTextColor(Color.parseColor("#FFFFFF"));
                    setNumberPickerTextColor(numberPicker, Color.parseColor("#FFFFFF"));
                    break;
            }

            textScore.setText(String.valueOf(gamer.getScore()));
            textPlace.setText(String.valueOf(gamer.getPlace()));

            numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                @Override
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    if (adapterListener != null) {
                        adapterListener.afterTextChanged(textName.getText().toString(), newVal);
                    }
                }
            });
        }
    }


    void setAdapterListener(@NonNull AdapterListener adapterListener){
        this.adapterListener = adapterListener;
    }


    public interface AdapterListener{
        void onItemLongClicked(@NonNull Gamer gamer);
        void afterTextChanged(String name, int points);
    }


    private static void setNumberPickerTextColor(NumberPicker numberPicker, int color)
    {

        try{
            Field selectorWheelPaintField = numberPicker.getClass()
                    .getDeclaredField("mSelectorWheelPaint");
            selectorWheelPaintField.setAccessible(true);
            ((Paint)selectorWheelPaintField.get(numberPicker)).setColor(color);
        }
        catch(NoSuchFieldException e){
            Log.w("PickerTextColor", e);
        }
        catch(IllegalAccessException e){
            Log.w("PickerTextColor", e);
        }
        catch(IllegalArgumentException e){
            Log.w("PickerTextColor", e);
        }

        final int count = numberPicker.getChildCount();
        for(int i = 0; i < count; i++){
            View child = numberPicker.getChildAt(i);
            if(child instanceof EditText)
                ((EditText)child).setTextColor(color);
        }
        numberPicker.invalidate();
    }


}
