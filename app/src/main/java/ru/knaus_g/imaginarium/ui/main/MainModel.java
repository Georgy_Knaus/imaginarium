package ru.knaus_g.imaginarium.ui.main;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import ru.knaus_g.imaginarium.model.Gamer;
import ru.knaus_g.imaginarium.room.DataBase;
import ru.knaus_g.imaginarium.utils.SingleLiveEvent;

public class MainModel extends ViewModel {

    @Nullable
    private Thread thread;

    @Nullable
    private Thread thread2;

    private SingleLiveEvent<Gamer> singleLiveEvent = new SingleLiveEvent<>();

    private final DataBase database;

    @Inject
    public MainModel(DataBase database) {
        this.database = database;
    }

    @NonNull
    LiveData<List<Gamer>> getMLiveData() {
        return database.daoGamer().getAllGamers();
    }


    Gamer getGamer() {

        final Gamer[] gamer = new Gamer[1];

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                gamer[0] = database.daoGamer().getGamer();
            }
        });
        thread.start();

        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return gamer[0];
    }


    void setAddPoints(String name, int points) {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                database.daoGamer().setAddPoints(points, name);
            }
        });
        thread.start();
    }

    void deleteItem(@NonNull final Integer id) {

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                database.daoGamer().deleteGamer(id);
            }
        });
        thread.start();
    }

    void addGamer(@NonNull Gamer gamer) {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                database.daoGamer().addGamer(gamer);
            }
        });
        thread.start();
    }

    void deleteAll() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                database.daoGamer().deleteAllGamer();
            }
        });
        thread.start();
    }

    void setScore() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                database.daoGamer().setPoints();
                database.daoGamer().setNullPoints();
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Gamer> array = database.daoGamer().getOrderList();

                for (int i = 0; i < array.size(); i++) {
                    array.get(i).setPlace(array.size() - i);
                }
                database.daoGamer().addAllGamer(array);
            }
        });
        thread2.start();
    }


    SingleLiveEvent<Gamer> getSingleLiveEvent() {
        return singleLiveEvent;
    }

    void setValueLiveEvent(Gamer gamer) {
        singleLiveEvent.setValue(gamer);
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        if (thread != null && thread.isAlive()) {
            thread.interrupt();
        }

        if (thread2 != null && thread2.isAlive()) {
            thread2.interrupt();
        }
    }

}
