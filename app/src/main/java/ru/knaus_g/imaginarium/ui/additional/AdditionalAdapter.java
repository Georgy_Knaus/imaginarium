package ru.knaus_g.imaginarium.ui.additional;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.knaus_g.imaginarium.R;
import ru.knaus_g.imaginarium.model.AdditionalObject;

public class AdditionalAdapter extends RecyclerView.Adapter<AdditionalAdapter.ViewHolder> {

    @NonNull
    private final List<AdditionalObject> array = new ArrayList<>();
    @Nullable
    private AdapterAdditionalListener adapterAdditionalListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.additional_adapter_item, parent, false));

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapterAdditionalListener != null) {
                    adapterAdditionalListener.onItemClick(array.get(vh.getAdapterPosition()));
                }
            }
        });

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(array.get(position));
    }

    @Override
    public int getItemCount() {
        return array.size();
    }

    public void addAll(List<AdditionalObject> list){
        array.clear();
        array.addAll(list);
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView textViewName;
        private TextView textView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            textViewName = itemView.findViewById(R.id.textName);
            textView = itemView.findViewById(R.id.text);
        }

        void bind(AdditionalObject additionalObject){

            Glide
                    .with(itemView)
                    .load(additionalObject.getImage())
                    .into(imageView);
            textView.setText(additionalObject.getText());
            textViewName.setText(additionalObject.getTextName());
        }
    }

    public void setAdapterAdditionalListener(@NonNull AdapterAdditionalListener adapterAdditionalListener){
        this.adapterAdditionalListener = adapterAdditionalListener;
    }



    public interface AdapterAdditionalListener{
        void onItemClick(AdditionalObject additionalObject);
    }

}
