package ru.knaus_g.imaginarium.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.Objects;

import ru.knaus_g.imaginarium.R;

public class DialogDeleteGamer extends AppCompatDialogFragment {

    @Nullable
    private DialogDeleteListener dialogDeleteListener;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        int id = getArguments().getInt("id");
        String name = getArguments().getString("name");


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);

        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_delete_gamer,
                getActivity().findViewById(R.id.layoutDialogDeleteGamer));


        builder.setView(view);

        TextView textTitle = view.findViewById(R.id.textTitle);
        textTitle.setText(getString(R.string.deleting));

        TextView textMessage = view.findViewById(R.id.textMessage);
        textMessage.setText(getString(R.string.sure_delete_gamer) + " " + name + "?");

        Button buttonNO = view.findViewById(R.id.buttonNO);
        buttonNO.setText(getString(R.string.cancel));

        Button buttonYES = view.findViewById(R.id.buttonYES);
        buttonYES.setText(getString(R.string.delete));

        ImageView imageIcon = view.findViewById(R.id.imageIcon);
        imageIcon.setImageResource(R.drawable.ic_warning_black_24dp);


        buttonYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogDeleteListener != null) {
                    dialogDeleteListener.DialogDeletePositiveClick(id);
                }
                dismiss();
            }
        });


        buttonNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();

        if(alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }

        return alertDialog;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            dialogDeleteListener = (DialogDeleteGamer.DialogDeleteListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    public interface DialogDeleteListener{
        void DialogDeletePositiveClick(int id);
    }
}
