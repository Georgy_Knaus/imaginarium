package ru.knaus_g.imaginarium.ui.additional;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import ru.knaus_g.imaginarium.R;
import ru.knaus_g.imaginarium.model.AdditionalObject;
import ru.knaus_g.imaginarium.viewmodels.ViewModelProviderFactory;

public class AdditionalActivity extends DaggerAppCompatActivity {

    @Inject
    ViewModelProviderFactory providerFactory;

    @NonNull
    private AdditionalAdapter adapter = new AdditionalAdapter();
    private AdditionalActivityModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.additional_content);
        super.onCreate(savedInstanceState);

        model = ViewModelProviders.of(this, providerFactory).get(AdditionalActivityModel.class);


        // ActionBar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.additional_content));
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);


            // RecyclerView
            RecyclerView recyclerView = findViewById(R.id.recyclerViewAdditionalContent);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);




            model.getLiveData().observe(this, new Observer<List<AdditionalObject>>() {
                @Override
                public void onChanged(List<AdditionalObject> additionalObjects) {
                    adapter.addAll(additionalObjects);
                }
            });
        }


        adapter.setAdapterAdditionalListener(new AdditionalAdapter.AdapterAdditionalListener() {
            @Override
            public void onItemClick(AdditionalObject additionalObject) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(additionalObject.getLink())));
            }
        });
    }




    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
