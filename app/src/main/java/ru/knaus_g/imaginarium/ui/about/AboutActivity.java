package ru.knaus_g.imaginarium.ui.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import dagger.android.support.DaggerAppCompatActivity;
import ru.knaus_g.imaginarium.R;
import ru.knaus_g.imaginarium.utils.Constants;

public class AboutActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.about_programmer);
        super.onCreate(savedInstanceState);


        // ActionBar
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }


        TextView textView = findViewById(R.id.text22);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.mosigra.ru/blog/id00000007174/")));
            }
        });

        TextView textView1 = findViewById(R.id.text3333);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType(Constants.type);
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] { Constants.email });
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.title_send));
                Intent chooseIntent =  Intent.createChooser(intent, getString(R.string.title_chooser));

                startActivity(chooseIntent);
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }
}
