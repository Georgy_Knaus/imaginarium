package ru.knaus_g.imaginarium.ui.main;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import java.util.List;
import java.util.Objects;

import ru.knaus_g.imaginarium.R;
import ru.knaus_g.imaginarium.model.Gamer;

public class DialogAddGamer extends AppCompatDialogFragment {


    private List<Gamer> listGamers;

    public DialogAddGamer(List<Gamer> listGamers){
        this.listGamers = listGamers;
    }

    public DialogAddGamer() {
    }

    @Nullable
    private DialogAddGamerListener dialogAddGamerListener;
    private String color;
    private EditText editTextName;


    public interface DialogAddGamerListener{
        void DialogAddGamerPositiveClick(String name, String color);
    }


    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_dialog_add_person,
                getActivity().findViewById(R.id.layoutDialogAddGamer));


        TextView textTitle = view.findViewById(R.id.textTitle);
        textTitle.setText(getString(R.string.add_gamer));

        TextView textMessage = view.findViewById(R.id.textChoose);
        textMessage.setText("Выберите цвет");

        editTextName = view.findViewById(R.id.editTextName);

        Button buttonNO = view.findViewById(R.id.buttonNO);
        buttonNO.setText(getString(R.string.cancel));
        buttonNO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Button buttonYES = view.findViewById(R.id.buttonYES);
        buttonYES.setText(getString(R.string.add));
        buttonYES.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogAddGamerListener != null && editTextName.getText().length() > 0) {
                    if(color != null){
                        dialogAddGamerListener.DialogAddGamerPositiveClick(editTextName.getText().toString(), color);
                        dismiss();
                    } else{
                        Toast.makeText(getContext(), "Выберите цвет", Toast.LENGTH_SHORT).show();
                    }
                } else{
                    Toast.makeText(getContext(), "Напишите имя игрока", Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView imageIcon = view.findViewById(R.id.imageIcon);
        imageIcon.setImageResource(R.drawable.ic_person_add_black_24dp);

        RadioGroup radioGroup = view.findViewById(R.id.radioGroup);

        RadioButton radioButtonOrange = view.findViewById(R.id.radioButton);
        RadioButton radioButtonBlue = view.findViewById(R.id.radioButton2);
        RadioButton radioButtonBlack = view.findViewById(R.id.radioButton3);
        RadioButton radioButtonWhite = view.findViewById(R.id.radioButton4);
        RadioButton radioButtonRed = view.findViewById(R.id.radioButton5);
        RadioButton radioButtonYellow = view.findViewById(R.id.radioButton6);
        RadioButton radioButtonGreen = view.findViewById(R.id.radioButton7);


        for(int i =0; i<listGamers.size(); i++){
            String color = listGamers.get(i).getColor();

            if(color.equals("#EF6C00")){
                radioButtonOrange.setVisibility(View.GONE);
            }

            if(color.equals("#2196f3")){
                radioButtonBlue.setVisibility(View.GONE);
            }

            if(color.equals("#212121")){
                radioButtonBlack.setVisibility(View.GONE);
            }

            if(color.equals("#FFFFFF")){
                radioButtonWhite.setVisibility(View.GONE);
            }

            if(color.equals("#b71c1c")){
                radioButtonRed.setVisibility(View.GONE);
            }

            if(color.equals("#FFEB3B")){
                radioButtonYellow.setVisibility(View.GONE);
            }

            if(color.equals("#388E3C")){
                radioButtonGreen.setVisibility(View.GONE);
            }
        }



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case -1:
                        break;
                    case R.id.radioButton:
                        color = "#EF6C00"; // Оранжевый
                        break;
                    case R.id.radioButton2:
                        color = "#2196f3"; // Синий
                        break;
                    case R.id.radioButton3:
                        color = "#212121"; // Черный
                        break;
                    case R.id.radioButton4:
                        color = "#FFFFFF"; // Белый
                        break;
                    case R.id.radioButton5:
                        color = "#b71c1c"; // Красный
                        break;
                    case R.id.radioButton6:
                        color = "#FFEB3B"; // Желтый
                        break;
                    case R.id.radioButton7:
                        color = "#388E3C"; // Зеленый
                        break;
            }
        }});


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        AlertDialog alertDialog = builder.create();

        if(alertDialog.getWindow() != null){
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        }
        return alertDialog;
    }




    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            dialogAddGamerListener = (DialogAddGamerListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}
