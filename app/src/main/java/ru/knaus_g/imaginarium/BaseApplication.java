package ru.knaus_g.imaginarium;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import ru.knaus_g.imaginarium.di.DaggerAppComponent;

public class BaseApplication extends DaggerApplication {


    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
