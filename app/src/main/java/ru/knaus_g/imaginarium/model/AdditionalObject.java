package ru.knaus_g.imaginarium.model;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "AdditionalContent")
public class AdditionalObject {

    @PrimaryKey
    @NonNull
    private String textName;
    private String text;
    private String image;
    private String link;


    @Ignore
    public AdditionalObject(@NonNull String textName, String text, String image, String link) {
        this.textName = textName;
        this.text = text;
        this.image = image;
        this.link = link;
    }

    public AdditionalObject(){}

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    @NonNull
    public String getTextName() {
        return textName;
    }

    public void setTextName(@NonNull String textName) {
        this.textName = textName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
