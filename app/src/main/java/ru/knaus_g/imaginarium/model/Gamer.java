package ru.knaus_g.imaginarium.model;


import android.widget.NumberPicker;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.Comparator;
import java.util.Objects;

@Entity(tableName = "Gamers")
public class Gamer {


    @PrimaryKey(autoGenerate = true)
    private int id;
    private int place;
    private String name;
    private String color;
    private int score;
    private int addPoints;
    @Ignore
    private NumberPicker numberPicker;


    public Gamer(String name, String color){
        this.name = name;
        this.color = color;
        this.score = 0;
        this.place = 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, color, score, /*addPoints,*/ place);
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Gamer that = (Gamer) obj;
        return id == that.id &&
                name.equals(that.name) &&
                color.equals(that.color) &&
                score == that.score &&
                place == that.place;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getAddPoints() {
        return addPoints;
    }

    public void setAddPoints(int addPoints) {
        this.addPoints = addPoints;
    }

    public NumberPicker getNumberPicker() {
        return numberPicker;
    }

    public void setNumberPicker(NumberPicker numberPicker) {
        this.numberPicker = numberPicker;
    }

    public static class GamerSort implements Comparator<Gamer> {
        @Override
        public int compare(Gamer gamer, Gamer gamer2) {
            return gamer.getScore() - (gamer2.getScore());
        }
    }
}
