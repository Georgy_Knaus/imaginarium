package ru.knaus_g.imaginarium;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;


import ru.knaus_g.imaginarium.ui.main.MainActivity;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        ImageView imageView = findViewById(R.id.imageView);
        TextView textView = findViewById(R.id.textSplashScreen);

        double random = Math.random();

        if(random<0.1){
            imageView.setImageResource(R.drawable.card_1_v);
        } else if(random>=0.1 && random<0.2){
            imageView.setImageResource(R.drawable.card_2_v);
        } else if(random>=0.2 && random<0.3){
            imageView.setImageResource(R.drawable.card_3_v);
        } else if(random>=0.3 && random<0.4){
            imageView.setImageResource(R.drawable.card_25_v);
        } else if(random>=0.4 && random<=0.5){
            imageView.setImageResource(R.drawable.card_5_v);
        } else if(random>=0.5 && random<=0.6){
            imageView.setImageResource(R.drawable.card_29_v);
        } else if(random>=0.6 && random<=0.7){
            imageView.setImageResource(R.drawable.card_7_v);
        } else if(random>=0.7 && random<=0.8){
            imageView.setImageResource(R.drawable.card_8_v);
        } else if(random>=0.8 && random<=0.9){
            imageView.setImageResource(R.drawable.card_9_v);
        } else if(random>=0.9 && random<=1){
            imageView.setImageResource(R.drawable.card_10);
        }


        if(random<0.1){
            textView.setText(R.string.splash_screen_1);
        } else if(random>=0.1 && random<0.2){
            textView.setText(R.string.splash_screen_1);
        } else if(random>=0.2 && random<0.3){
            textView.setText(R.string.splash_screen_4);
        } else if(random>=0.3 && random<0.4){
            textView.setText(R.string.splash_screen_4);
        } else if(random>=0.4 && random<=0.5){
            textView.setText(R.string.splash_screen_5);
        } else if(random>=0.5 && random<=0.6){
            textView.setText(R.string.splash_screen_5);
        } else if(random>=0.6 && random<=0.7){
            textView.setText(R.string.splash_screen_7);
        } else if(random>=0.7 && random<=0.8){
            textView.setText(R.string.splash_screen_7);
        } else if(random>=0.8 && random<=0.9){
            textView.setText(R.string.splash_screen_6);
        } else if(random>=0.9 && random<=1){
            textView.setText(R.string.splash_screen_6);
        }


        int SPLASH_TIME = 3500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        }, SPLASH_TIME);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
