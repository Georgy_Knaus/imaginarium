package ru.knaus_g.imaginarium.room;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import ru.knaus_g.imaginarium.model.Gamer;

@Dao
public interface DaoGamer {

    // Методы
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addGamer(Gamer gamer);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addAllGamer(List<Gamer> list);

    @Query("SELECT * FROM Gamers")
    LiveData<List<Gamer>> getAllGamers();

    @Query("SELECT * FROM Gamers LIMIT 1")
    Gamer getGamer();

    @Query("DELETE FROM Gamers WHERE id =:id")
    void deleteGamer(int id);

    @Query("DELETE FROM Gamers")
    void deleteAllGamer();

    @Query("UPDATE Gamers SET addPoints =:value WHERE name =:name")
    void setAddPoints(int value, String name);


    @Query("Select * from Gamers order by score")
    List<Gamer> getOrderList();


    @Query("UPDATE Gamers SET score = addPoints+score")
    void setPoints();

    @Query("UPDATE Gamers SET addPoints = 0")
    void setNullPoints();
}
