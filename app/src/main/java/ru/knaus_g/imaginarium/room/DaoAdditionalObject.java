package ru.knaus_g.imaginarium.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import ru.knaus_g.imaginarium.model.AdditionalObject;

@Dao
public interface DaoAdditionalObject {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void addAllContent(List<AdditionalObject> list);


    @Query("SELECT * FROM AdditionalContent")
    LiveData<List<AdditionalObject>> getAllContent();
}
