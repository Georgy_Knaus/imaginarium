package ru.knaus_g.imaginarium.room;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import ru.knaus_g.imaginarium.model.AdditionalObject;
import ru.knaus_g.imaginarium.model.Gamer;

@Database(entities = {Gamer.class, AdditionalObject.class}, version = 1, exportSchema = false)
public abstract class DataBase extends RoomDatabase {

    public abstract DaoGamer daoGamer();
    public abstract DaoAdditionalObject daoAdditionalObject();
}
