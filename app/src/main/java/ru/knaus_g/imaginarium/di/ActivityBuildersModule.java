package ru.knaus_g.imaginarium.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.knaus_g.imaginarium.di.additional.AdditionalScope;
import ru.knaus_g.imaginarium.di.additional.AdditionalViewModelModule;
import ru.knaus_g.imaginarium.di.main.MainModule;
import ru.knaus_g.imaginarium.di.main.MainScope;
import ru.knaus_g.imaginarium.di.main.MainViewModelModule;
import ru.knaus_g.imaginarium.ui.about.AboutActivity;
import ru.knaus_g.imaginarium.ui.additional.AdditionalActivity;
import ru.knaus_g.imaginarium.ui.main.MainActivity;
import ru.knaus_g.imaginarium.ui.regulations.RegulationsActivity;
import ru.knaus_g.imaginarium.ui.result.ResultActivity;

@Module
public abstract class ActivityBuildersModule {


    @MainScope
    @ContributesAndroidInjector(modules = {MainViewModelModule.class, MainModule.class})
    abstract MainActivity contributeMainActivity();


    @AdditionalScope
    @ContributesAndroidInjector(modules = {AdditionalViewModelModule.class,})
    abstract AdditionalActivity contributeAdditionalActivity();


    @ContributesAndroidInjector()
    abstract AboutActivity contributeAboutActivity();


    @ContributesAndroidInjector()
    abstract ResultActivity contributeResultActivity();


    @ContributesAndroidInjector()
    abstract RegulationsActivity contributeRegulationsActivity();

}
