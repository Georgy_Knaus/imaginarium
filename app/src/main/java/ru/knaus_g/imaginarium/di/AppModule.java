package ru.knaus_g.imaginarium.di;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.knaus_g.imaginarium.room.DataBase;

@Module
public class AppModule {

    @Singleton
    @Provides
    public static DataBase getDataBase(Application application) {
        return androidx.room.Room.databaseBuilder(application.getApplicationContext(),
                            DataBase.class, "data").addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);

                            // Дополнения
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Одисея', 'По сравнению с другими дополнениями карточки «Одиссеи» выдержаны в более спокойных тонах', 'android.resource://ru.knaus_g.imaginarium/drawable/odisea', 'https://imaginarium-game.ru/catalog/dopolneniya-imaginarium/dopolnenie-odisseya/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Пандора', 'Тут котики и лошади, Карабас-Барабас и женщина, которая держит весь мир, свидание симпатичных мух и очаровательная дракониха', 'android.resource://ru.knaus_g.imaginarium/drawable/pandora', 'https://imaginarium-game.ru/catalog/dopolneniya-imaginarium/dopolnenie-pandora/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Ариадна', 'Картинки с большим количеством деталей и мелочей. По цветовой гамме чуть спокойнее «Пандоры», но все так же красочны и приятны', 'android.resource://ru.knaus_g.imaginarium/drawable/ariadna', 'https://imaginarium-game.ru/catalog/dopolneniya-imaginarium/dopolnenie-ariadna/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Химера 18+', 'Открывает мир страхов, фобий и пороков. Жуткие, откровенные, отвратительные, но вместе с тем – красивые и пугающе-притягательные иллюстрации', 'android.resource://ru.knaus_g.imaginarium/drawable/himera', 'https://imaginarium-game.ru/catalog/dopolneniya-imaginarium/dopolnenie-khimera/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Касиопея', 'В новом дополнении 98 карт с совершенно новыми иллюстрациями, большинства из которых и в сети-то нет, тем более в Имаджинариуме и других дополнениях', 'android.resource://ru.knaus_g.imaginarium/drawable/kasiopea', 'https://www.mosigra.ru/Face/Show/kassiopeia/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Персефона', 'Много ярких и красочных карт, которые отлично подойдут для игры с детьми. Непохожие друг на друга иллюстрации — и смешные, и задумчивые — из разных эпох', 'android.resource://ru.knaus_g.imaginarium/drawable/persefona', 'https://imaginarium-game.ru/catalog/dopolneniya-imaginarium/dopolnenie-persefona/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Гармония', 'Дополнение «Имаджинариум: Гармония» поможет узнать больше о мире, в котором живут дети', 'android.resource://ru.knaus_g.imaginarium/drawable/harmony', 'https://www.mosigra.ru/Face/Show/imaginarium_garmonia/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Прайм-Тайм', 'Позволяет играть в привычную игру по-новому, и открывает огромный простор для ассоциаций, если игроки хоть немного знакомы с современной культурой', 'android.resource://ru.knaus_g.imaginarium/drawable/prime', 'https://www.mosigra.ru/Face/Show/imaginarium_praim_taim/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Дорожно-ремонтный набор', 'Как следует из названия, этот набор нужен либо для дороги, либо для ремонта. Логично, правда? Но есть у него еще и третья, секретная функция', 'android.resource://ru.knaus_g.imaginarium/drawable/road', 'https://imaginarium-game.ru/catalog/dopolneniya-imaginarium/dorozhno-remontnyy-nabor/')");

                            // Большие издания
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Имаджинариум', 'Это настольная игра в которой потребуется только интуиция и воображение. Здесь не важна ваша эрудиция, возраст или статус', 'android.resource://ru.knaus_g.imaginarium/drawable/main', 'https://imaginarium-game.ru/catalog/versii-imaginarium/imaginarium/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Имаджинариум Детство', 'Карты очень добрые, светлые с приятными детскими рисунками. Игрушечные утята, девочки-близняшки, осьминоги зайчики и песочные замки.', 'android.resource://ru.knaus_g.imaginarium/drawable/mainchildren', 'https://imaginarium-game.ru/catalog/versii-imaginarium/imaginarium-detstvo/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Имаджинариум Союзмультфильм', 'Здесь собраны все наши любимые с детства персонажи из старых добрых советских мультфильмов! Море удовольствия и ностальгии', 'android.resource://ru.knaus_g.imaginarium/drawable/mainmult', 'https://imaginarium-game.ru/catalog/versii-imaginarium/imaginarium-soyuzmultfilm/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Имаджинариум 3D', 'Это версия игры с 3D-очками и специальными картами, которые становятся объемными, когда вы надеваете эти самые 3D-очки', 'android.resource://ru.knaus_g.imaginarium/drawable/main3d', 'https://imaginarium-game.ru/catalog/versii-imaginarium/imaginarium-3d/')");
                            db.execSQL("INSERT INTO AdditionalContent (textName, text, image, link) VALUES ('Имаджинариум Юбилейный', 'Это специальное коллекционное издание, посвященное 5-летию настольной игры «Имаджинариум»', 'android.resource://ru.knaus_g.imaginarium/drawable/maingold', 'https://imaginarium-game.ru/catalog/versii-imaginarium/imadzhinarium-5-let/')");
                        }
                    })
                            .build();
            }

    }


