package ru.knaus_g.imaginarium.di.main;

import androidx.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import ru.knaus_g.imaginarium.di.ViewModelKey;
import ru.knaus_g.imaginarium.ui.main.MainModel;

@Module
public abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainModel.class)
    public abstract ViewModel bindMainModel(MainModel mainModel);

}
