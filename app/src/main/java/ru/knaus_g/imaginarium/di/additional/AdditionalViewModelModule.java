package ru.knaus_g.imaginarium.di.additional;


import androidx.lifecycle.ViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import ru.knaus_g.imaginarium.di.ViewModelKey;
import ru.knaus_g.imaginarium.ui.additional.AdditionalActivityModel;

@Module
public abstract class AdditionalViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AdditionalActivityModel.class)
    public abstract ViewModel bindAdditionalActivityModel(AdditionalActivityModel model);
}
