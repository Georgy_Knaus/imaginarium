package ru.knaus_g.imaginarium.di.main;

import dagger.Module;
import dagger.Provides;
import ru.knaus_g.imaginarium.ui.main.MainAdapter;

@Module
public abstract class MainModule {


    @Provides
    @MainScope
    static MainAdapter getMainAdapter(){
        return new MainAdapter();
    }
}
